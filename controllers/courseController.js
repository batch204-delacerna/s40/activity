const Course = require("../models/Course");


//Create a New Course - Solution 1
module.exports.addCourse = (reqBody) => {

	// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
	// Uses the information from the request body to provide all the necessary information
	let newCourse = new Course({

		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});

	//console.log(reqBody);

	// Saves the created object to our database
	return newCourse.save().then((course, error) => {

		// Course creation failed
		if(error) {
			return false

		// Course creation successful
		} else {
			return true
		}

	})

}

//Create a New Course - Solution 2
// module.exports.addCourse = (reqBody, userData) => {

//     return User.findById(userData.userId).then(result => {

//         if (userData.isAdmin == false) {
//             return false
//         } else {
//             let newCourse = new Course({
//                 name: reqBody.name,
//                 description: reqBody.description,
//                 price: reqBody.price
//             })
        
//             //Saves the created object to the database
//             return newCourse.save().then((course, error) => {
//                 //Course creation failed
//                 if(error) {
//                     return false
//                 } else {
//                     //course creation successful
//                     return "Course creation successful"
//                 }
//             })
//         }
        
//     });    
// }



//Retrieve all courses
module.exports.getAllCourses = () => {

	return Course.find({}).then(result => {
		return	result;
	});
}


//Controller for retrieving all ACTIVE courses
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	});
}


//Retrieving a specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	});
}


// Controller for updating a record
// Updating course
module.exports.updateCourse = (reqParams, reqBody, data) => {
	
	if (data.isAdmin === true) {
		// Specify the fields/properties of the document to be updated
		let updatedCourse = {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		}
	
		// Syntax:
		// findByIdAndUpdate(document ID, updatesToBeApplied)
		return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
			// Course not updated
			if (error) {
				return false

			// Course updated successfully
			} else {
				return true
			}
		});

	} else {
		return false
	}
}


// S40 ACTIVITY

module.exports.archiveCourse = (reqParams, isAdmin, reqBody) => {

	if (isAdmin) {

		let courseUpdates = {
			isActive: reqBody.isActive
		}

		return Course.findByIdAndUpdate(reqParams.courseId, courseUpdates).then((course, error) => {

			if (error) {
				return false
			} else {
				return true
			}
		})
	} else {
		return false
	}

}