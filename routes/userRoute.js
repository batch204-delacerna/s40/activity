const express = require("express");
const router = express.Router();
// import userController.js
const userController = require("../controllers/userController");
const auth = require("../auth");

// Route for checking if the user's email already exist in the database
router.post("/checkEmail", (req, res) => {

	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));

});

// Route for User Registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for User Authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// ======= ACTIVITY FOR S38 =========
// Route to get USer document by its ID
// The "auth.verity" acts as a middleware to ensure that the user is logged in before they can retrieve details
router.post("/details", auth.verify, (req,res) => {
	
	// Uses the "decode" method defined in the "auth.js" file to retrieve the user information from the token passing the "token" from the request header as an argument
	const userData = auth.decode(req.headers.authorization);
	/*
		{
		  id: '634015c341c2e3e5a5c3a08e',
		  email: 'jane@gmail.com',
		  isAdmin: false,
		  iat: 1665407164
		}

	*/
	console.log(userData);
	console.log(req.headers.authorization);

	/*userController.getProfile(userData).then(resultFromController => res.send(resultFromController));*/
	
	// Provides the user's ID for the getProfile controller method
	userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController));
}); 

module.exports = router;